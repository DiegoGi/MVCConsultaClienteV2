﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Modelo
{
    public class clsModelo
    {
        #region Definicion de Variables
        SqlConnection cnnBaseDatos = null;
        SqlCommand cmdConsCliente = null;
        SqlDataReader drConsCliente = null;
        DataTable dttConsCliente = null;
        String cadenaConexion = null;
        #endregion

        public clsModelo() {
         cadenaConexion = "Data Source=LAPTOP-MOK4HOTB\\SQLEXPRESS ; Initial Catalog =dbClientes ; Integrated Security=true";
        }

        #region 
        public DataTable mtConsultaCliente(int recibeCedula)
        {
            cnnBaseDatos = new SqlConnection(cadenaConexion);
            cmdConsCliente = new SqlCommand();
            dttConsCliente = new DataTable();

            cmdConsCliente.Connection = cnnBaseDatos;
            cmdConsCliente.CommandType = CommandType.Text;
            cmdConsCliente.CommandText = "select Nombre,Apellido,Edad,Telefono,Direccion from dblClientes where Cedula="+recibeCedula;

            cnnBaseDatos.Open();
            drConsCliente = cmdConsCliente.ExecuteReader();
            dttConsCliente.Load(drConsCliente);
            cnnBaseDatos.Close();


            return dttConsCliente;
        }
        #endregion
    }
}
