﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controlador;

namespace Vista
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            mtConsCliente();
        }

        private void mtConsCliente()
        {
            clsVariables objVariables = new clsVariables();
            objVariables.Cedula =Convert.ToInt32( txtCedula.Text);
            clsControlador objControlador = new clsControlador(objVariables);
            objControlador.mtConsultaClientes();
            lblNombre.Text = objVariables.Nombre;
            lblApellido.Text = objVariables.Apellido;
            lblEdad.Text = objVariables.Edad.ToString();
            lblTelefono.Text = objVariables.Telefono.ToString();
            lblDireccion.Text = objVariables.Direccion;
        }
    }
}
