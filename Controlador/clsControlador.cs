﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo;
using System.Data;

namespace Controlador
{
    public class clsControlador
    {
        #region Variables
        clsVariables objVariables = null;
        DataTable dttConsClientes = null;
        #endregion

        #region Constructor
        public clsControlador(clsVariables objVar)
        {
            objVariables = objVar;
        }
        #endregion

        public void mtConsultaClientes()
        {
            clsModelo clsDatos = new clsModelo();
            dttConsClientes = clsDatos.mtConsultaCliente(objVariables.Cedula);
            objVariables.Nombre = dttConsClientes.Rows[0]["Nombre"].ToString();
            objVariables.Apellido = dttConsClientes.Rows[0]["Apellido"].ToString();
            objVariables.Edad =Convert.ToInt32( dttConsClientes.Rows[0]["Edad"]);
            objVariables.Telefono = Convert.ToInt32(dttConsClientes.Rows[0]["Telefono"]);
            objVariables.Direccion = dttConsClientes.Rows[0]["Direccion"].ToString();

            /*  if (Convert.ToInt32(objVariables.Edad) >= 17)
            {
                objVariables.Nombre = "Homero";
                objVariables.Apellido = "Thomson";
                objVariables.Telefono = "Mayor de Edad";}*/


        }
    }
}


